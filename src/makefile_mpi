# compiler
FC = mpif90
#FC = gfortran

# compiler flags
# standard compilation
STDFLAGS  = -cpp -O3 -xHost -fp-model source -ftz
STDFLAGS += -fpe0 -fno-alias -no-prec-div -no-prec-sqrt -ip
# debug compilation
DBGFLAGS  = -cpp -ftrapuv -C -assume ieee_fpe_flags -fp-speculation=off -init=snan,arrays -g -r8
DBGFLAGS += -traceback -debug -debug-parameters all -debug inline_debug_info -check all
DBGFLAGS += -fpe0 -fno-alias -no-prec-div -no-prec-sqrt -ip
DBGFLAGS += -diag-disable 10182
#DBGFLAGS += -warn unused
# debug flags for gfortran compiler
#FCFLAGS = -Og -g -Wall -Wextra -pedantic -fcheck=all -fbacktrace

MPIFLAGS  = -D__parallel -DMPI_REAL=MPI_DOUBLE_PRECISION -DMPI_2REAL=MPI_2DOUBLE_PRECISION
NCFLAGS   = -D__netcdf
NCFLAGS  += -D__netcdf4
NCFLAGS  += -D__netcdf4_parallel

# include paths
INCLUDES = -I/muksoft/packages/netcdf4_hdf5parallel/4411c_443f/hdf5-1.10.0-patch1/mvapich2-2.3rc1/intel/2018.1.163/include/
#INCLUDES = -I/muksoft/packages/netcdf4_hdf5parallel/4411c_443f/hdf5-1.10.0-patch1/mvapich2-2.3rc1/gnu/include/

# library paths
LFLAGS = -L/muksoft/packages/netcdf4_hdf5parallel/4411c_443f/hdf5-1.10.0-patch1/mvapich2-2.3rc1/intel/2018.1.163/lib64/
#LFLAGS = -L/muksoft/packages/netcdf4_hdf5parallel/4411c_443f/hdf5-1.10.0-patch1/mvapich2-2.3rc1/gnu/lib64/

# libraries to be linked
LIBS = -lnetcdff

# list of source files
SRCS = data_output_binary_module.f90 \
       data_output_module.f90 \
       mod_kinds.f90 \
       module_a_mod.f90 \
       modules.f90 \
       data_output_netcdf4_parallel_module.f90 \
       data_output_netcdf4_serial_module.f90 \
       test_output.f90

# name of executable
PROG=test_output

# executable directory
BIN = ../bin

#-----#-----#-----#-----#
SUF = _parallel
# set compiler flags depending on make goal
ifeq ($(MAKECMDGOALS),debug)
	FCFLAGS = $(DBGFLAGS)
	SUF = _parallel_debug
else
	FCFLAGS = $(STDFLAGS)
endif

# add output path for .mod files
FCFLAGS += -module $(OBJ)

# add MPI flags
FCFLAGS += $(MPIFLAGS)

# add netcdf flag
FCFLAGS += $(NCFLAGS)

# directory for object files and module files
OBJ = $(BIN)/objects$(SUF)
# actual name of executable to be build
MAIN = $(PROG:%=$(BIN)/%$(SUF))
# list of object files
OBJS = $(SRCS:%.f90=$(OBJ)/%.o)

#-----#-----#-----#-----#

.PHONY: depend clean

.SUFFIXES: .o .f90

all: $(MAIN)
	@echo "Build successful"

$(MAIN): $(OBJS)
	$(FC) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

$(OBJS): $(OBJ)/%.o: %.f90
	@echo Compiling $<
	$(FC) $(FCFLAGS) $(INCLUDES) -c $< -o $@

clean:
	$(RM) $(MAIN) $(OBJ)/*.o $(OBJ)/*.mod *.bak
	$(RM) $(MAIN)_debug $(OBJ)_debug/*

debug: $(MAIN)
	@echo "Debug build successful"

depend: $(SRCS)
	makedepend $(INCLUDES) $^

#-----#-----#-----#-----#
# Dependencies
$(OBJ)/data_output_binary_module.o:\
	$(OBJ)/mod_kinds.o
$(OBJ)/data_output_module.o: \
	$(OBJ)/data_output_binary_module.o \
	$(OBJ)/data_output_netcdf4_parallel_module.o \
	$(OBJ)/data_output_netcdf4_serial_module.o \
  $(OBJ)/mod_kinds.o
$(OBJ)/mod_kinds.o:
$(OBJ)/modules.o: \
	$(OBJ)/mod_kinds.o
$(OBJ)/module_a_mod.o: \
	$(OBJ)/data_output_module.o \
	$(OBJ)/mod_kinds.o \
	$(OBJ)/modules.o
$(OBJ)/data_output_netcdf4_parallel_module.o:\
	$(OBJ)/mod_kinds.o
$(OBJ)/data_output_netcdf4_serial_module.o:\
	$(OBJ)/mod_kinds.o
$(OBJ)/test_output.o: \
	$(OBJ)/data_output_module.o \
	$(OBJ)/mod_kinds.o \
	$(OBJ)/module_a_mod.o \
	$(OBJ)/modules.o

# DO NOT DELETE
