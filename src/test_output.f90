PROGRAM test_output
! Reads data from a reference NetCDF file and output these in a new NetCDF file
! using the new data-output module.
! @author: Tobias Gronemeier
! @date:   2018-10-08 (start of development)

#if defined( __nopointer )
   USE arrays_3d, &
      ONLY: u
#else
   USE arrays_3d, &
      ONLY: u, u_1
#endif

   USE averaging, &
      ONLY: u_av

   USE control_parameters, &
      ONLY: simulated_time

   USE data_output_module, &
      ONLY: dom_database_debug_output, dom_init, dom_get_error_message, &
            dom_def_att, dom_def_dim, dom_def_file, dom_def_var, &
            dom_write_var, dom_finalize_output, dom_start_output

   USE indices

   USE kinds

   USE module_a_mod, &
      ONLY: moda_init, moda_init_output, moda_input, moda_output

#if defined( __parallel )
#if defined( __mpifh )
    INCLUDE "mpif.h"
#else
    USE MPI
#endif
#endif

#if defined( __netcdf )
   USE NETCDF
#endif

   USE pegrid

   IMPLICIT NONE

   CHARACTER(LEN=800) :: error_message = ''                     !< error message
   CHARACTER(LEN=200) :: namelist_file = './test_output_p3d'    !< name of namelist file
   CHARACTER(LEN=200) :: reference_dir = './reference_data/'    !< name of directory containing reference datasets
   CHARACTER(LEN=200), DIMENSION(4) :: reference_file = &       !< name of files containing reference datasets
      (/ 'test_urban_3d.nc   ', &
         'test_urban_av_3d.nc', &
         'test_urban_pr.nc   ', &
         'test_urban_ts.nc   ' /)

   CHARACTER(LEN=50)  :: output_file_format = 'netcdf4-serial' ! 'netcdf4-serial' ! 'netcdf4-parallel' ! 'binary'  !< default file format for output files

   CHARACTER(LEN=50), DIMENSION(10) :: data_output = ' '


   INTEGER(iwp) :: return_value = 0   !< return value; if /=0, error occured
   INTEGER(iwp) :: i         !< loop index
   INTEGER(iwp) :: nt = 1    !< number of time steps within reference data
   INTEGER(iwp) :: t         !< time index

   LOGICAL ::  debug_output = .TRUE.  !< if true, additional debug output is given

   LOGICAL, DIMENSION(7) ::  do_output = (/ .TRUE., &    !< make 3d data output
                                            .TRUE., &    !< make mask data output
                                            .TRUE., &    !< make xy data output
                                            .TRUE., &    !< make xz data output
                                            .TRUE., &    !< make yz data output
                                            .TRUE., &    !< make ts data output
                                            .TRUE.  /)   !< make pr data output

   REAL(wp), TARGET :: umax !< maximum of u (time-series output)

   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET :: time !< values of dimension time
   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET :: u_pr !< values of u profile
   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET :: x    !< values of dimension x
   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET :: xu   !< values of dimension xu
   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET :: y    !< values of dimension y
   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET :: zu   !< values of dimension zu


   !
   !-- MPI initialisation. comm2d is preliminary set, because
   !-- it will be defined in init_pegrid but is used before in cpu_log.
#if defined( __parallel )
   CALL MPI_INIT( ierr )

   CALL MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
   CALL MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
#endif
   WRITE (myid_char,'(''_'',I6.6)')  myid

   CALL init(return_value)
   IF (return_value /= 0) THEN
      WRITE(*,'(A,I3)') '++  Error while initializing! Error code', return_value
      STOP
   ENDIF

   DO  i = 1, 4
      reference_file(i) = TRIM(reference_dir)//TRIM(reference_file(i))
   ENDDO

   CALL read_data(reference_file, 'init', 0, return_value)
   IF (return_value /= 0) THEN
      WRITE(*,'(A,I3)') '++  Error while read reference header! Error code', return_value
      STOP
   ENDIF

   CALL init_output
   IF (return_value /= 0) THEN
      WRITE(*,'(A,I3)') '++  Error during init_output! Error code ', return_value
      CALL dom_get_error_message( error_message )
      WRITE(*,'(A)') TRIM( error_message )
      STOP
   ENDIF
   ! Print debug information
   ! @TODO remove later
   ! IF ( myid == 0 )  CALL dom_debug

   ! loop over all time steps
   DO  t = 0, nt-1

      simulated_time = time(t)
      WRITE(*,'(A,I3,A,F11.3)') '--  time step: ', t, ',   time: ', simulated_time

      CALL read_data(reference_file, 'read', t, return_value)
      IF (return_value /= 0) THEN
         WRITE(*,'(A,I3)') '++  Error while read reference! Error code ', return_value
         STOP
      ENDIF

      CALL make_output( t )
      IF (return_value /= 0) THEN
         WRITE(*,'(A,I3)') '++  Error during make_output! Error code ', return_value
         CALL dom_get_error_message( error_message )
         WRITE(*,'(A)') TRIM( error_message )
         STOP
      ENDIF

   ENDDO

   CALL close_output
   IF (return_value /= 0) THEN
      WRITE(*,'(A,I3)') '++  Error during close_output! Error code ', return_value
      STOP
   ENDIF


#if defined( __parallel )
   CALL MPI_FINALIZE( ierr )
#endif


CONTAINS

   SUBROUTINE init(return_value)
      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: return_value

      NAMELIST /main_par/ data_output, nx, ny, nz, reference_file

      WRITE(*,*) '   Open namelist file "'//TRIM(namelist_file)//'"'
      OPEN(100, FILE=TRIM(namelist_file), STATUS='old', ACTION='read', IOSTAT=return_value)
      IF ( return_value /= 0 ) THEN; return_value = 1; RETURN; ENDIF

      READ(100, main_par, IOSTAT=return_value)
      IF ( return_value /= 0 ) THEN; return_value = 2; RETURN; ENDIF

#if defined(__parallel)
      IF ( numprocs == 1 )  THEN
         nxl = 0; nxr = nx
         nys = 0; nyn = ny
      ELSEIF ( numprocs == 2 )  THEN
         IF ( myid == 0 )  THEN
            nxl = 0; nxr = nx
            nys = 0; nyn = ny / 2
         ELSEIF ( myid == 1 )  THEN
            nxl = 0; nxr = nx
            nys = ny / 2 + 1; nyn = ny
         ENDIF
      ELSEIF ( numprocs == 4 )  THEN
         IF ( myid == 0 )  THEN
            nxl = 0; nxr = nx / 2
            nys = 0; nyn = ny / 2
         ELSEIF ( myid == 1 )  THEN
            nxl = nx / 2 + 1; nxr = nx
            nys = 0; nyn = ny / 2
         ELSEIF ( myid == 2 )  THEN
            nxl = 0; nxr = nx / 2
            nys = ny / 2 + 1; nyn = ny
         ELSEIF ( myid == 3 )  THEN
            nxl = nx / 2 + 1; nxr = nx
            nys = ny / 2 + 1; nyn = ny
         ENDIF
      ELSE
         WRITE(*,*) '   Error: number of PEs not supported!'
         return_value = 1_iwp
         RETURN
      ENDIF
#else
      nxl = 0; nxr = nx
      nys = 0; nyn = ny
#endif
      nzb = 0; nzt = nz
      nxlg = nxl-nbgp
      nxrg = nxr+nbgp
      nysg = nys-nbgp
      nyng = nyn+nbgp

#if defined (__nopointer)
      ALLOCATE( u(nzb:nzt+1,nysg:nyng,nxlg:nxrg) )
#else
      ALLOCATE( u_1(nzb:nzt+1,nysg:nyng,nxlg:nxrg) )
      u => u_1
#endif

      ALLOCATE( u_av(nzb:nzt+1,nysg:nyng,nxlg:nxrg) )

      ALLOCATE( u_pr(nzb:nzt+1) )

      ALLOCATE( x(0:nx) )
      ALLOCATE( y(0:ny) )
      ALLOCATE( xu(0:nx) )
      ALLOCATE( zu(0:nz+1) )

      ! Initialize whole array; if any of these values appear in the output
      ! file, there is an error somewhere.
      u(:,:,:) = -555.0_wp
      x(:) = -555.0_wp
      y(:) = -555.0_wp
      zu(:) = -555.0_wp

      CALL moda_init(100, return_value)
      IF ( return_value /= 0 ) THEN; RETURN; ENDIF

      CLOSE(100, IOSTAT=return_value)
      IF ( return_value /= 0 ) THEN; return_value = 3; RETURN; ENDIF

   END SUBROUTINE init

   ! read reference dataset
   SUBROUTINE read_data(filename, mode, t, return_value)

      IMPLICIT NONE

      CHARACTER(LEN=*) :: mode !< either 'init', 'read' or 'close'

      CHARACTER(LEN=*), DIMENSION(4), INTENT(IN) :: filename !< name of input file

      INTEGER, INTENT(INOUT) :: return_value !< return value
      INTEGER :: i           !< loop index
      INTEGER :: id_dim_time !< dimension if of time
      INTEGER :: id_file     !< file id of single file
      INTEGER :: id_var_time !< variable id for time
      INTEGER :: id_var_u    !< variable id for u (3d)
      INTEGER :: id_var_uav  !< variable id for u (3d_av)
      INTEGER :: id_var_umax !< variable id for umax (ts)
      INTEGER :: id_var_upr  !< variable id for u (pr)
      INTEGER :: id_var_x    !< variable id for x (3d)
      INTEGER :: id_var_y    !< variable id for y (3d)
      INTEGER :: id_var_z    !< variable id for z (3d)
      INTEGER :: j           !< loop index
      INTEGER :: k           !< loop index
      INTEGER :: nc_stat     !< netcdf return status
      INTEGER :: t           !< time index

      INTEGER, DIMENSION(4) :: id_file_all     !< file ids of all reference files

      REAL(wp), DIMENSION(:), ALLOCATABLE :: data_in_1d     !< array for input data
      REAL(wp), DIMENSION(:,:,:), ALLOCATABLE :: data_in_3d !< array for input data

      SAVE id_file_all, id_var_time, id_var_u, id_var_uav, id_var_umax, &
           id_var_upr, &
           id_var_x, id_var_y, id_var_z

#if defined( __netcdf )
      IF ( mode == 'init' )  THEN

         ! Open all reference files and save file ids
         DO  i = 1, 4
            nc_stat = NF90_OPEN( TRIM(filename(i)), NF90_NOWRITE, id_file )
            IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 1; RETURN; ENDIF
            id_file_all(i) = id_file
         ENDDO

         id_file = id_file_all(1)

         nc_stat = NF90_INQ_DIMID( id_file, "time", id_dim_time )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 11; RETURN; ENDIF
         nc_stat = NF90_INQ_VARID( id_file, "time", id_var_time )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 12; RETURN; ENDIF
         nc_stat = NF90_INQUIRE_DIMENSION( id_file, id_dim_time, len=nt )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 13; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file, "x", id_var_x )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 2; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file, "y", id_var_y )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 3; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file, "zu_3d", id_var_z )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 4; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file, "u", id_var_u )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 5; RETURN; ENDIF

         ALLOCATE( time(0:nt-1) )
         nc_stat = NF90_GET_VAR( id_file, id_var_time, time, (/1/), count=(/nt/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 6; RETURN; ENDIF

         nc_stat = NF90_GET_VAR( id_file, id_var_x, x, (/1/), count=(/nx+1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 6; RETURN; ENDIF
         xu = x - ( x(1) - x(0) ) * 0.5

         nc_stat = NF90_GET_VAR( id_file, id_var_y, y, (/1/), count=(/ny+1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 7; RETURN; ENDIF

         nc_stat = NF90_GET_VAR( id_file, id_var_z, zu, (/1/), count=(/nz+2/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 8; RETURN; ENDIF

         ! Get IDs of averaged data
         nc_stat = NF90_INQ_VARID( id_file_all(2), "u", id_var_uav )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 5; RETURN; ENDIF

         ! Get IDs of profile data
         nc_stat = NF90_INQ_VARID( id_file_all(3), "u", id_var_upr )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 5; RETURN; ENDIF

         ! Get IDs of time series data
         nc_stat = NF90_INQ_VARID( id_file_all(4), "umax", id_var_umax )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 5; RETURN; ENDIF

         CALL moda_input(id_file_all, 'init', 0, return_value)
         IF ( return_value /= 0 ) RETURN

      ELSEIF ( mode == 'read' )  THEN

         ALLOCATE( data_in_3d(0:nx,0:ny,nzb:nzt+1) )

         ! Read 3d data
         nc_stat = NF90_GET_VAR( id_file_all(1), id_var_u, data_in_3d, (/1,1,1,t+1/), &
                                 count=(/nx+1, ny+1, nz+2, 1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 9; RETURN; ENDIF

         DO i = nxl, nxr
            DO j = nys, nyn
               DO k = nzb, nzt+1
                  u(k,j,i) = data_in_3d(i,j,k)
               ENDDO
            ENDDO
         ENDDO

         ! Read averaged 3d data
         nc_stat = NF90_GET_VAR( id_file_all(2), id_var_uav, data_in_3d, (/1,1,1,t+1/), &
                                 count=(/nx+1, ny+1, nz+2, 1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 9; RETURN; ENDIF

         DO i = nxl, nxr
            DO j = nys, nyn
               DO k = nzb, nzt+1
                  u_av(k,j,i) = data_in_3d(i,j,k)
               ENDDO
            ENDDO
         ENDDO

         DEALLOCATE( data_in_3d )

         ! Read profile data
         nc_stat = NF90_GET_VAR( id_file_all(3), id_var_upr, u_pr, (/1, t+1/), &
                                 count=(/nz+2, 1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 9; RETURN; ENDIF

         ALLOCATE( data_in_1d(1) )

         ! Read time-series data
         nc_stat = NF90_GET_VAR( id_file_all(4), id_var_umax, data_in_1d, (/t+1/), &
                                 count=(/1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 9; RETURN; ENDIF
         umax = data_in_1d(1)

         DEALLOCATE( data_in_1d )

         CALL moda_input(id_file_all, 'read', t, return_value)
         IF ( return_value /= 0 ) RETURN

      ELSEIF ( mode == 'close' )  THEN

         DO  i = 1, 4
            nc_stat = NF90_CLOSE( id_file_all(i) )
            IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 10; RETURN; ENDIF
         ENDDO

      ENDIF
#else
      return_value = 1
#endif

   ENDSUBROUTINE


   ! initialize output
   SUBROUTINE init_output
      IMPLICIT NONE

      CHARACTER(LEN=50) :: filename          !< name of output file

      LOGICAL, DIMENSION(0:nx)    :: x_mask_ma !< mask for x dimesion, masked output
      LOGICAL, DIMENSION(0:nx)    :: x_mask_yz !< mask for x dimesion, yz output
      LOGICAL, DIMENSION(0:ny)    :: y_mask_ma !< mask for y dimesion, masked output
      LOGICAL, DIMENSION(0:ny)    :: y_mask_xz !< mask for y dimesion, xz output
      LOGICAL, DIMENSION(0:nzt+1) :: z_mask_ma !< mask for z dimesion, masked output
      LOGICAL, DIMENSION(0:nzt+1) :: z_mask_xy !< mask for z dimesion, xy output

      INTEGER(iwp) :: i !< loop index
      ! INTEGER(iwp), DIMENSION(0:nx) :: x_int !< dummy dimension

      x_mask_ma(:) = .FALSE.
      DO  i = 0, nx, 4
         x_mask_ma(i) = .TRUE.
      ENDDO

      y_mask_ma = .FALSE.
      DO  i = 0, ny, 4
         y_mask_ma(i) = .TRUE.
      ENDDO

      x_mask_yz = .FALSE.
      x_mask_yz(0) = .TRUE.
      x_mask_yz(10) = .TRUE.

      y_mask_xz = .FALSE.
      y_mask_xz(10) = .TRUE.

      z_mask_ma = .FALSE.
      z_mask_ma(3) = .TRUE.
      z_mask_ma(5) = .TRUE.

      z_mask_xy = .FALSE.
      z_mask_xy(1)  = .TRUE.
      z_mask_xy(10) = .TRUE.

      CALL dom_init( program_debug_output_unit=6, debug_output=debug_output )

      !------------------------------
      ! Initialize 3D data output
      IF ( do_output(1) )  THEN
         filename = 'DATA_OUTPUT_3D'
         return_value = dom_def_file(filename, output_file_format)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_att(filename, name='Conventions', value='COARDS')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  run: test_urban.00', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  host: p6imuk', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  07-11-18 15:58:30', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='VAR_LIST', value=';u;t_soil;')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'time', output_type='real64', bounds=(/0_iwp,1_iwp/), values_realwp=(/0._wp/))
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'x', values_realwp=x, bounds=(/0,nx/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='x', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'xu', values_realwp=xu, bounds=(/0,nx/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='xu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'y', values_realwp=y, bounds=(/0,ny/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='y', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'zu_3d', values_realwp=(/0.0_wp/), bounds=(/0,nz+1/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='zu_3d', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN


         return_value = dom_def_var(filename, 'u', (/'xu   ', 'y    ', 'zu_3d', 'time '/), output_type='real32')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='units', value='m/s')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='long_name', value='u')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='_FillValue', value=-999.0_4)
         IF ( return_value /= 0 ) RETURN
      ENDIF

      !------------------------------
      ! Initialize masked data output
      IF ( do_output(2) )  THEN
         filename = 'DATA_OUTPUT_MASKED_M01'
         return_value = dom_def_file(filename, output_file_format)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'time', values_realwp=(/0._wp,0._wp/), bounds=(/0_iwp,1_iwp/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'xu', values_realwp=xu, bounds=(/0,nx/), output_type='real64', mask=x_mask_ma)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='xu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'y', values_realwp=y, bounds=(/0,ny/), output_type='real64', mask=y_mask_ma)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='y', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'zu_3d', values_realwp=zu, bounds=(/0,nz+1/), output_type='real64', mask=z_mask_ma)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='zu_3d', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_var(filename, 'u', (/'xu   ', 'y    ', 'zu_3d', 'time '/), output_type='real32')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='units', value='m/s')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='long_name', value='u')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='_FillValue', value=-999.0_4)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_att(filename, name='Conventions', value='COARDS')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  run: test_urban.00', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  host: p6imuk', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  07-11-18 15:58:30', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='VAR_LIST', value=';u;')
         IF ( return_value /= 0 ) RETURN
      ENDIF

      !------------------------------
      ! Initialize 2D data output
      IF ( do_output(3) )  THEN
         filename = 'DATA_OUTPUT_XY'
         return_value = dom_def_file(filename, output_file_format)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'time', values_realwp=(/0._wp/), bounds=(/0_iwp,1_iwp/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'xu', values_realwp=xu, bounds=(/0,nx/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='xu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'y', values_realwp=y, bounds=(/0,ny/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='y', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'zu_xy', values_realwp=zu, bounds=(/0,nz+1/), output_type='real64', mask=z_mask_xy)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='zu_xy', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_var(filename, 'u_xy', (/'xu   ', 'y    ', 'zu_xy', 'time '/), output_type='real32')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_xy', name='units', value='m/s')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_xy', name='long_name', value='u_xy')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_xy', name='_FillValue', value=-999.0_4)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_att(filename, name='Conventions', value='COARDS')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  run: test_urban.00', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  host: p6imuk', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  07-11-18 15:58:30', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='VAR_LIST', value=';u_xy;')
         IF ( return_value /= 0 ) RETURN
      ENDIF

      IF ( do_output(4) )  THEN
         filename = 'DATA_OUTPUT_XZ'
         return_value = dom_def_file(filename, output_file_format)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'time', values_realwp=(/0._wp/), bounds=(/0_iwp,1_iwp/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'xu', values_realwp=xu, bounds=(/0,nx/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='xu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'y_xz', values_realwp=y, bounds=(/0,ny/), mask=y_mask_xz, output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='y_xz', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'zu', values_realwp=zu, bounds=(/0,nz+1/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='zu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_var(filename, 'u_xz', (/'xu  ', 'y_xz', 'zu  ', 'time'/), output_type='real32')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_xz', name='units', value='m/s')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_xz', name='long_name', value='u_xz')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_xz', name='_FillValue', value=-999.0_4)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_att(filename, name='Conventions', value='COARDS')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  run: test_urban.00', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  host: p6imuk', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  07-11-18 15:58:30', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='VAR_LIST', value=';u_xz;')
         IF ( return_value /= 0 ) RETURN
      ENDIF

      IF ( do_output(5) )  THEN
         filename = 'DATA_OUTPUT_YZ'
         return_value = dom_def_file(filename, output_file_format)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'time', values_realwp=(/0._wp/), bounds=(/0_iwp,1_iwp/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'xu_yz', values_realwp=xu, bounds=(/0,nx/), mask=x_mask_yz, output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='xu_yz', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'y', values_realwp=y, bounds=(/0,ny/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='y', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'zu', values_realwp=zu, bounds=(/0,nz+1/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='zu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_var(filename, 'u_yz', (/'xu_yz', 'y    ', 'zu   ', 'time '/), output_type='real32')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_yz', name='units', value='m/s')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_yz', name='long_name', value='u_yz')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u_yz', name='_FillValue', value=-999.0_4)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_att(filename, name='Conventions', value='COARDS')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  run: test_urban.00', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  host: p6imuk', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  07-11-18 15:58:30', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='VAR_LIST', value=';u_yz;')
         IF ( return_value /= 0 ) RETURN
      ENDIF

      !------------------------------
      if ( myid == 0 )  THEN
         ! Initialize time series output
         IF ( do_output(6) )  THEN
            filename = 'DATA_OUTPUT_TS'
            return_value = dom_def_file(filename, output_file_format)
            IF ( return_value /= 0 ) RETURN

            return_value = dom_def_att(filename, name='Conventions', value='COARDS')
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500  run: test_urban.00')
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, name='title', value='  host: p6imuk  07-11-18 15:58:30', append=.TRUE.)
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, name='VAR_LIST', value=';umax;ghf')
            IF ( return_value /= 0 ) RETURN

            return_value = dom_def_dim(filename, 'time', bounds=(/0_iwp/), output_type='real64')
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
            IF ( return_value /= 0 ) RETURN

            return_value = dom_def_var(filename, 'umax', (/'time'/), output_type='real32', is_global=.TRUE.)
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, variable='umax', name='units', value='m/s')
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att(filename, variable='umax', name='long_name', value='umax')
            IF ( return_value /= 0 ) RETURN
         ENDIF
      ENDIF

      ! Initialize profile output
      IF ( do_output(7) )  THEN
         filename = 'DATA_OUTPUT_PR'
         return_value = dom_def_file(filename, output_file_format)
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_att(filename, name='Conventions', value='COARDS')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='rotation_angle', value=0._wp)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='PALM 6.0  Rev: 3500  run: test_urban.00')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value='  host: p6imuk  07-11-18 15:58:30', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='title', value=',    18.0 s average', append=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='time_avg', value='   18.0 s avg')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='no_rows', value=3)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='no_columns', value=2)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='cross_profiles', value=';u;t_soil;')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, name='VAR_LIST', value=';u;t_soil;')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_dim(filename, 'time', values_realwp=(/0._wp/), bounds=(/0_iwp,1_iwp/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='time', name='units', value='seconds')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_dim(filename, 'zu', values_realwp=zu, bounds=(/LBOUND(zu), UBOUND(zu)/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='zu', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_var(filename, 'u', (/'zu  ', 'time'/), output_type='real32', is_global=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='units', value='m/s')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att(filename, variable='u', name='long_name', value='u')
         IF ( return_value /= 0 ) RETURN
      ENDIF


      !------------------------------
      ! Initialize module output
      CALL moda_init_output(return_value, do_output)
      IF ( return_value /= 0 ) RETURN
#if defined( __parallel )
      CALL MPI_Barrier( MPI_COMM_WORLD, ierr)
#endif

      ! End file initialization and start with output
      return_value = dom_start_output()
      IF ( return_value /= 0 ) RETURN
#if defined( __parallel )
      CALL MPI_Barrier( MPI_COMM_WORLD, ierr)
#endif

   ENDSUBROUTINE

   ! write data to output file
   SUBROUTINE make_output( t )
      IMPLICIT NONE

      INTEGER(iwp), INTENT(IN) :: t !< time index

      REAL(wp), TARGET :: simulated_time_local !< local simulated time

      REAL(wp), POINTER :: out_0d_pointer                   !< points to 0d output array

      REAL(wp), DIMENSION(:), POINTER :: out_1d_pointer     !< points to 1d output array

      REAL(wp), DIMENSION(:,:,:), POINTER :: out_3d_pointer !< points to 3d output array


      simulated_time_local = simulated_time

      ! 3D output
      IF ( do_output(1) )  THEN
         out_0d_pointer => simulated_time_local
         return_value = dom_write_var('DATA_OUTPUT_3D', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
         IF ( return_value /= 0 ) RETURN

         out_1d_pointer => zu
         return_value = dom_write_var('DATA_OUTPUT_3D', 'zu_3d', var_realwp_1d=out_1d_pointer, bounds_start=(/0/), bounds_end=(/nz+1/) )
         IF ( return_value /= 0 ) RETURN

         out_3d_pointer => u
         return_value = dom_write_var('DATA_OUTPUT_3D', 'u', var_realwp_3d=out_3d_pointer, bounds_start=(/nxl, nys, nzb, t/), bounds_end=(/nxr, nyn, nzt+1, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! masked output
      IF ( do_output(2) )  THEN
         out_0d_pointer => simulated_time_local
         return_value = dom_write_var('DATA_OUTPUT_MASKED_M01', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
         IF ( return_value /= 0 ) RETURN

         out_3d_pointer => u
         return_value = dom_write_var('DATA_OUTPUT_MASKED_M01', 'u', var_realwp_3d=out_3d_pointer, bounds_start=(/nxl, nys, nzb, t/), bounds_end=(/nxr, nyn, nzt+1, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! xy output
      IF ( do_output(3) )  THEN
         out_0d_pointer => simulated_time_local
         return_value = dom_write_var('DATA_OUTPUT_XY', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
         IF ( return_value /= 0 ) RETURN

         out_3d_pointer => u
         return_value = dom_write_var('DATA_OUTPUT_XY', 'u_xy', var_realwp_3d=out_3d_pointer, bounds_start=(/nxl, nys, nzb, t/), bounds_end=(/nxr, nyn, nzt+1, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! xz output
      IF ( do_output(4) )  THEN
         out_0d_pointer => simulated_time_local
         return_value = dom_write_var('DATA_OUTPUT_XZ', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
         IF ( return_value /= 0 ) RETURN

         out_3d_pointer => u
         return_value = dom_write_var('DATA_OUTPUT_XZ', 'u_xz', var_realwp_3d=out_3d_pointer, bounds_start=(/nxl, nys, nzb, t/), bounds_end=(/nxr, nyn, nzt+1, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! yz output
      IF ( do_output(5) )  THEN
         out_0d_pointer => simulated_time_local
         return_value = dom_write_var('DATA_OUTPUT_YZ', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
         IF ( return_value /= 0 ) RETURN

         out_3d_pointer => u
         return_value = dom_write_var('DATA_OUTPUT_YZ', 'u_yz', var_realwp_3d=out_3d_pointer, bounds_start=(/nxl, nys, nzb, t/), bounds_end=(/nxr, nyn, nzt+1, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! time-series output
      IF ( myid == 0 )  THEN
         IF ( do_output(6) )  THEN
            out_0d_pointer => simulated_time_local
            return_value = dom_write_var('DATA_OUTPUT_TS', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
            IF ( return_value /= 0 ) RETURN

            out_0d_pointer => umax
            return_value = dom_write_var('DATA_OUTPUT_TS', 'umax', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
            IF ( return_value /= 0 ) RETURN
         ENDIF
      ENDIF

      ! profile output
      IF ( do_output(7) )  THEN
         out_0d_pointer => simulated_time_local
         return_value = dom_write_var('DATA_OUTPUT_PR', 'time', var_realwp_0d=out_0d_pointer, bounds_start=(/t/), bounds_end=(/t/) )
         IF ( return_value /= 0 ) RETURN

         out_1d_pointer => u_pr
         return_value = dom_write_var('DATA_OUTPUT_PR', 'u', var_realwp_1d=out_1d_pointer, bounds_start=(/nzb, t/), bounds_end=(/nzt+1, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! Output for other modules
      CALL moda_output( t, return_value, do_output )
      IF ( return_value /= 0 ) RETURN

   ENDSUBROUTINE

   ! close all opened output files
   SUBROUTINE close_output
      IMPLICIT NONE

      return_value = dom_finalize_output()
      IF ( return_value /= 0 ) RETURN

   ENDSUBROUTINE

ENDPROGRAM test_output
