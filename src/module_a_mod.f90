MODULE module_a_mod
! dummy module for testing in- and output
! @author: Tobias Gronemeier
! @date:   2018-10-08 (start of development)

   USE data_output_module, &
      ONLY: dom_def_att, dom_def_dim, dom_def_file, dom_def_var, dom_write_var

   USE indices

   USE kinds

#if defined( __netcdf )
   USE NETCDF
#endif

   USE pegrid, &
      ONLY: myid

   IMPLICIT NONE

   PRIVATE

   CHARACTER(LEN=50), DIMENSION(10) :: data_output = ' '

   INTEGER(iwp) :: nt = 1   !< number of time steps within reference data
   INTEGER(iwp) :: nzs = 7  !< number of soil levels
   INTEGER(iwp) :: nzb_soil !< lowest soil level
   INTEGER(iwp) :: nzt_soil !< heighest soil level

   REAL(wp), TARGET :: ghf          !< ground-heat flux (time-series output)

   REAL(wp), DIMENSION(:), ALLOCATABLE :: zs !< soil levels

#if defined (__nopointer)
   REAL(wp), DIMENSION(:,:,:), ALLOCATABLE, TARGET ::  t_soil !< soil temperature
#else
   REAL(wp), DIMENSION(:,:,:), ALLOCATABLE, TARGET ::  t_soil_1 !< soil temperature
   REAL(wp), DIMENSION(:,:,:), POINTER, CONTIGUOUS ::  t_soil   !< pointer: soil temperature
#endif

   REAL(wp), DIMENSION(:), ALLOCATABLE, TARGET ::  t_soil_pr !< profile of soil temperature

   REAL(wp), DIMENSION(:,:,:), ALLOCATABLE, TARGET ::  t_soil_av !< averaged soil temperature

   SAVE

   INTERFACE moda_init
      MODULE PROCEDURE moda_init
   END INTERFACE moda_init

   INTERFACE moda_init_output
      MODULE PROCEDURE moda_init_output
   END INTERFACE moda_init_output

   INTERFACE moda_input
      MODULE PROCEDURE moda_input
   END INTERFACE moda_input

   INTERFACE moda_output
      MODULE PROCEDURE moda_output
   END INTERFACE moda_output

   PUBLIC moda_init, moda_init_output, moda_input, moda_output

CONTAINS


   ! initialize module a
   SUBROUTINE moda_init(id_file, return_value)
      IMPLICIT NONE

      INTEGER(iwp), INTENT(INOUT) :: return_value     !< return value
      INTEGER(iwp), INTENT(IN)    :: id_file !< id of namelist file

      NAMELIST /module_a_par/ data_output, nzs

      READ(id_file, module_a_par, IOSTAT=return_value)
      IF ( return_value /= 0 ) THEN; return_value = 102; RETURN; ENDIF

      nzb_soil = 0
      nzt_soil = nzb_soil + nzs

#if defined (__nopointer)
      ALLOCATE( t_soil(nzb_soil:nzt_soil,nysg:nyng,nxlg:nxrg) )
#else
      ALLOCATE( t_soil_1(nzb_soil:nzt_soil,nysg:nyng,nxlg:nxrg) )
      t_soil => t_soil_1
#endif

      ALLOCATE( t_soil_av(nzb_soil:nzt_soil,nysg:nyng,nxlg:nxrg) )
      ALLOCATE( t_soil_pr(nzb_soil:nzt_soil) )
      ALLOCATE( zs(nzb_soil:nzt_soil) )

      ! initialize array with dummy value
      t_soil = -555.0_wp
      zs = -555.0_wp

   END SUBROUTINE moda_init

   ! read input
   SUBROUTINE moda_input(id_file_all, mode, t, return_value)

      IMPLICIT NONE

      CHARACTER(LEN=*) :: mode !< either 'init' or 'read'

      INTEGER(iwp) :: return_value            !< return value
      INTEGER(iwp) :: i              !< loop index
      INTEGER(iwp) :: id_var_ghf     !< variable id for ghf (ts)
      INTEGER(iwp) :: id_var_tsoil   !< variable id for t_soil (3d)
      INTEGER(iwp) :: id_var_tsoilav !< variable id for t_soil (3d)
      INTEGER(iwp) :: id_var_tsoilpr !< variable id for t_soil (pr)
      INTEGER(iwp) :: id_var_zs      !< variable id for zs (3d)
      INTEGER(iwp) :: j              !< loop index
      INTEGER(iwp) :: k              !< loop index
      INTEGER(iwp) :: nc_stat        !< netcdf return status
      INTEGER(iwp) :: t              !< time index

      INTEGER(iwp), DIMENSION(4) :: id_file_all  !< file ids of all reference files

      REAL(wp), DIMENSION(:), ALLOCATABLE :: data_in_1d     !< array for input data
      REAL(wp), DIMENSION(:,:,:), ALLOCATABLE :: data_in_3d !< array for input data

      SAVE id_var_ghf, id_var_tsoil, id_var_tsoilav, id_var_tsoilpr, id_var_zs


#if defined( __netcdf )
      IF ( mode == 'init' )  THEN
         nc_stat = NF90_INQ_VARID( id_file_all(1), "zs_3d", id_var_zs )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 101; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file_all(1), "t_soil", id_var_tsoil )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 102; RETURN; ENDIF

         nc_stat = NF90_GET_VAR( id_file_all(1), id_var_zs, zs, (/1/), count=(/nzs+1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 103; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file_all(2), "t_soil", id_var_tsoilav )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 102; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file_all(3), "t_soil", id_var_tsoilpr )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 102; RETURN; ENDIF

         nc_stat = NF90_INQ_VARID( id_file_all(4), "ghf", id_var_ghf )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 102; RETURN; ENDIF

      ELSEIF ( mode == 'read' )  THEN

         ALLOCATE( data_in_3d(0:nx,0:ny,nzb_soil:nzt_soil) )

         ! Read 3d data
         nc_stat = NF90_GET_VAR( id_file_all(1), id_var_tsoil, data_in_3d, (/1,1,1,t+1/), &
                                 count=(/nx+1, ny+1, nzs+1, 1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 104; RETURN; ENDIF

         DO i = nxl, nxr
            DO j = nys, nyn
               DO k = nzb_soil, nzt_soil
                  t_soil(k,j,i) = data_in_3d(i,j,k)
               ENDDO
            ENDDO
         ENDDO

         ! Read averaged 3d data
         nc_stat = NF90_GET_VAR( id_file_all(2), id_var_tsoilav, data_in_3d, (/1,1,1,t+1/), &
                                 count=(/nx+1, ny+1, nzs+1, 1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 104; RETURN; ENDIF

         DO i = nxl, nxr
            DO j = nys, nyn
               DO k = nzb_soil, nzt_soil
                  t_soil_av(k,j,i) = data_in_3d(i,j,k)
               ENDDO
            ENDDO
         ENDDO

         DEALLOCATE( data_in_3d )

         ! Read profile data
         nc_stat = NF90_GET_VAR( id_file_all(3), id_var_tsoilpr, t_soil_pr, (/1, t+1/), &
                                 count=(/nzs+1, 1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 104; RETURN; ENDIF

         ALLOCATE( data_in_1d(1) )

         ! Read time-series data
         nc_stat = NF90_GET_VAR( id_file_all(4), id_var_ghf, data_in_1d, (/t+1/), &
                                 count=(/1/) )
         IF ( nc_stat /= NF90_NOERR ) THEN; return_value = 104; RETURN; ENDIF
         ghf = data_in_1d(1)

         DEALLOCATE( data_in_1d )

      ENDIF

      return_value = 0
#else
      return_value = 1
#endif

   END SUBROUTINE moda_input


   !------------------------------------------------------------------------------!
   ! Description:
   ! ------------
   !> Initialize output for module_a_mod.
   !------------------------------------------------------------------------------!
   SUBROUTINE moda_init_output(return_value, do_output)

      IMPLICIT NONE

      INTEGER(iwp) :: return_value          !< return value
      
      LOGICAL, DIMENSION(7), INTENT(IN) :: do_output  !< switch for each output file

      return_value = dom_def_file( 'DATA_OUTPUT_MODA' )
      IF ( return_value /= 0 ) RETURN

      IF ( do_output(1) )  THEN
         return_value = dom_def_dim('DATA_OUTPUT_3D', 'zs_3d', values_realwp=zs, bounds=(/0,nzs/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_3D', variable='zs_3d', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN

         return_value = dom_def_var('DATA_OUTPUT_3D', 't_soil', (/'x    ', 'y    ', 'zs_3d', 'time '/), output_type='real32')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_3D', variable='t_soil', name='units', value='K')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_3D', variable='t_soil', name='long_name', value='t_soil')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_3D', variable='t_soil', name='_FillValue', value=-999.0_4)
         IF ( return_value /= 0 ) RETURN
      ENDIF

      IF ( myid == 0 )  THEN
         IF ( do_output(6) )  THEN
            return_value = dom_def_var('DATA_OUTPUT_TS', 'ghf', (/'time'/), output_type='real32', is_global=.TRUE.)
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att('DATA_OUTPUT_TS', variable='ghf', name='units', value='W/m2')
            IF ( return_value /= 0 ) RETURN
            return_value = dom_def_att('DATA_OUTPUT_TS', variable='ghf', name='long_name', value='ghf')
            IF ( return_value /= 0 ) RETURN
         ENDIF
      ENDIF

      IF ( do_output(7) )  THEN
         return_value = dom_def_dim('DATA_OUTPUT_PR', 'zt_soil', values_realwp=zs, bounds=(/0,nzs/), output_type='real64')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_PR', variable='zt_soil', name='units', value='meters')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_var('DATA_OUTPUT_PR', 't_soil', (/'zt_soil', 'time   '/), output_type='real32', is_global=.TRUE.)
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_PR', variable='t_soil', name='units', value='K')
         IF ( return_value /= 0 ) RETURN
         return_value = dom_def_att('DATA_OUTPUT_PR', variable='t_soil', name='long_name', value='t_soil')
         IF ( return_value /= 0 ) RETURN
      ENDIF

   END SUBROUTINE moda_init_output


   ! make output
   SUBROUTINE moda_output( t, return_value, do_output )

      IMPLICIT NONE

      INTEGER(iwp), INTENT(INOUT) :: return_value  !< return value

      INTEGER(iwp), INTENT(IN) :: t       !< time index

      LOGICAL, DIMENSION(7), INTENT(IN) :: do_output  !< switch for each output file

      ! REAL(wp), DIMENSION(1), TARGET :: out_0d              !< dummy array with TARGET attribute

      REAL(wp), POINTER :: out_0d_pointer                   !< points to 0d output array

      REAL(wp), DIMENSION(:), POINTER :: out_1d_pointer     !< points to 1d output array

      REAL(wp), DIMENSION(:,:,:), POINTER :: out_3d_pointer !< points to 3d output array


      ! 3D output
      IF ( do_output(1) )  THEN
         out_3d_pointer => t_soil
         return_value = dom_write_var('DATA_OUTPUT_3D', 't_soil', var_realwp_3d=out_3d_pointer, &
                             bounds_start=(/nxl, nys, nzb_soil, t/), &
                             bounds_end=(/nxr, nyn, nzt_soil, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

      ! time-series output
      IF ( myid == 0 )  THEN
         IF ( do_output(6) )  THEN
            out_0d_pointer => ghf
            return_value = dom_write_var('DATA_OUTPUT_TS', 'ghf', var_realwp_0d=out_0d_pointer, &
                                bounds_start=(/t/), &
                                bounds_end=(/t/) )
            IF ( return_value /= 0 ) RETURN
         ENDIF
      ENDIF

      ! profile output
      IF ( do_output(7) )  THEN
         out_1d_pointer => t_soil_pr
         return_value = dom_write_var('DATA_OUTPUT_PR', 't_soil', var_realwp_1d=out_1d_pointer, &
                             bounds_start=(/nzb_soil, t/), &
                             bounds_end=(/nzt_soil, t/) )
         IF ( return_value /= 0 ) RETURN
      ENDIF

   END SUBROUTINE moda_output

END MODULE module_a_mod
